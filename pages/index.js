import HomeLayout from '@layout/Home'
import HomePage from '@components/HomePage'
import { DataProvider } from '@context/index'

export default function Home() {
  return (
    <>
      <DataProvider>
        <HomeLayout>
          <HomePage />
        </HomeLayout>
      </DataProvider>
    </>
  )
}
