import Footer from '@/components/Footer'
import NavBar from '@components/NavBar'
import Head from 'next/head'

const HomeLayout = ({ children }) => {
  return (
    <div className="text-gray-700">
      <Head>
        <title>GuRu Reader</title>
        <link rel="icon" href="favicon.ico" />
      </Head>
      <div
        className="min-h-screen mx-auto p-6 space-y-12 pb-16 w-11/12 md:w-5/6"
        style={{ marginBottom: '-48px' }}
      >
        <NavBar />
        <div>{children}</div>
      </div>
      <Footer />
    </div>
  )
}

export default HomeLayout
