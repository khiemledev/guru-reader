import { useState, createContext } from 'react'

const DataContext = createContext()

const DataProvider = ({ children }) => {
  const [selectedImage, setSelectedImage] = useState(null)
  const [resultImage, setResultImage] = useState(null)
  const [extractedInfo, setExtractedInfo] = useState(null)
  const [docTypeId, setDocTypeId] = useState('chung')
  const [processing, setProcessing] = useState(false)
  const [errorMessage, setErrorMessage] = useState(null)
  const value = {
    selectedImage,
    setSelectedImage,
    resultImage,
    setResultImage,
    extractedInfo,
    setExtractedInfo,
    docTypeId,
    setDocTypeId,
    processing,
    setProcessing,
    errorMessage,
    setErrorMessage,
  }

  return <DataContext.Provider value={value}>{children}</DataContext.Provider>
}

export { DataProvider, DataContext }
