import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_SERVICE_URL || 'http://192.168.28.23:8080',
  headers: {
    Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN}`
  }
})

export default instance