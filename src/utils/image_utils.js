import { fromImage } from 'imtool'

const encodeImageBase64 = async (image) => {
  const reader = new FileReader()
  reader.readAsDataURL(image)
  return new Promise((resolve, reject) => {
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.onerror = (error) => {
      reject(error)
    }
  })
}

const cropImage = async (image, bb) => {
  if (!bb) return image
  let img = await fromImage(image)
  if (bb.length === 4) {
    img = img.crop(bb[0], bb[1], bb[2] - bb[0], bb[3] - bb[1])
  } else if (bb.length === 8) {
    let minx = Math.min(bb[0], bb[2], bb[4], bb[6])
    let maxx = Math.max(bb[0], bb[2], bb[4], bb[6])
    let miny = Math.min(bb[1], bb[3], bb[5], bb[7])
    let maxy = Math.max(bb[1], bb[3], bb[5], bb[7])
    img = img.crop(minx, miny, maxx - minx, maxy - miny)
  }
  if (img.height < 50) {
    const f = 1.5
    img = img.scale(img.width * f, img.height * f)
  }
  return img.toFile()
}

const drawBBox = async (image, bboxes, texts) => {
  if (!bboxes || bboxes.length === 0) return image
  if (texts && bboxes.length !== texts.length) return image
  const img = await fromImage(image)
  const canvas = await img.toCanvas()
  const ctx = canvas.getContext('2d')
  for (let i = 0; i < bboxes.length; i++) {
    let bb = bboxes[i]
    ctx.beginPath()
    ctx.strokeStyle = 'green'
    ctx.lineWidth = 2
    // Draw rectangle
    // if (bb.length === 4) {
    //   ctx.strokeRect(bb[0], bb[1], bb[2] - bb[0], bb[3] - bb[1])
    // } else if (bb.length === 8) {
    //   let minx = Math.min(bb[0], bb[2], bb[4], bb[6])
    //   let maxx = Math.max(bb[0], bb[2], bb[4], bb[6])
    //   let miny = Math.min(bb[1], bb[3], bb[5], bb[7])
    //   let maxy = Math.max(bb[1], bb[3], bb[5], bb[7])
    //   ctx.strokeRect(minx, miny, maxx - minx, maxy - miny)
    // }
    // ctx.stroke()

    // Draw polygon
    ctx.beginPath()
    ctx.moveTo(bb[0], bb[1])
    ctx.lineTo(bb[2], bb[3])
    ctx.lineTo(bb[4], bb[5])
    ctx.lineTo(bb[6], bb[7])
    ctx.closePath()
    ctx.stroke()

    // Draw text
    if (texts) {
      let { width, height } = img
      let size = Math.min(width, height) * 0.05
      customElements
      ctx.font = `bold ${size}px Arial`
      ctx.fillStyle = 'yellow'
      ctx.fillText(texts[i], bb[0] - 5, bb[1])
    }
  }
  return await (await fromImage(canvas.toDataURL())).toFile()
}

const decodeImageBase64 = async (base64) => {
  let image = new Image()
  image.src = `data:image/jpeg;base64,${base64}`
  image = await fromImage(image)
  return await image.toBlob()
}

export { encodeImageBase64, decodeImageBase64, cropImage, drawBBox }
