import { useContext } from 'react'
import { DataContext } from '@context/index'

const ReceiptResult = () => {
  const { selectedImage, resultImage, extractedInfo } = useContext(DataContext)

  const fields = [
    {
      name: 'address',
      label: 'Địa chỉ',
    },
    {
      name: 'seller',
      label: 'Người bán',
    },
    {
      name: 'time',
      label: 'Thời gian',
    },
    {
      name: 'total',
      label: 'Tổng',
    },
  ]

  return (
    <>
      <div className="space-y-12">
        <div className="text-xl text-center">Kết quả</div>
        <table className="mx-auto text-lg">
          <tbody>
            {fields.map(({ name, label }, index) => {
              if (extractedInfo[name]) {
                return (
                  <tr key={index}>
                    <td
                      className="p-2 pr-5 text-right font-bold"
                      style={{ minWidth: '150px' }}
                    >
                      {label}
                    </td>
                    <td className="p-2 text-left">{extractedInfo[name]}</td>
                  </tr>
                )
              }
              return null
            })}
          </tbody>
        </table>
      </div>

      {resultImage ? (
        <div>
          <p className="text-center font-semibold text-xl mb-8">
            Ảnh trích xuất được
          </p>
          <div className="space-y-1">
            <div className="flex justify-center md:px-8">
              <img alt="Result image" src={URL.createObjectURL(resultImage)} />
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center md:p-8">
          <img alt="Result image" src={URL.createObjectURL(selectedImage)} />
        </div>
      )}
    </>
  )
}

export default ReceiptResult
