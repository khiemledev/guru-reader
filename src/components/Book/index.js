import { DataContext } from '@context/index'
import { useContext, useEffect } from 'react'

const Book = () => {
  const { selectedImage, resultImage, extractedInfo } = useContext(DataContext)

  useEffect(() => {
    console.log(extractedInfo)
  }, [])

  return (
    <>
      <div className='space-y-12'>
        <div className='text-xl text-center'>Kết quả</div>
      </div>
    </>
  )
}

export default Book
