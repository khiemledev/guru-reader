import { DataContext } from '@context/index'
import { useContext } from 'react'

const CongVan = () => {
  const { selectedImage, resultImage, extractedInfo } = useContext(DataContext)

  const fields = [
    {
      name: 'sokyhieu',
      label: 'Số ký hiệu',
    },
    {
      name: 'ngaybanhanh',
      label: 'Ngày ban hành',
    },
    {
      name: 'ngaycohieuluc',
      label: 'Ngày có hiệu lực',
    },
    {
      name: 'loaivanban',
      label: 'Loại văn bản',
    },
    {
      name: 'coquanbanhanh',
      label: 'Cơ quan ban hành',
    },
    {
      name: 'nguoiky',
      label: 'Người ký',
    },
    {
      name: 'noidung',
      label: 'Nội dung',
    },
  ]

  return (
    <>
      <div className="space-y-12">
        <div className="text-xl text-center">Kết quả</div>
        <table className="mx-auto text-lg">
          <tbody>
            {fields.map(({ name, label }, index) => {
              if (extractedInfo[name]) {
                return (
                  <tr key={index}>
                    <td
                      className="p-2 pr-5 text-right font-bold"
                      style={{ minWidth: '150px' }}
                    >
                      {label}
                    </td>
                    <td className="p-2 text-left">{extractedInfo[name]}</td>
                  </tr>
                )
              }
              return null
            })}
          </tbody>
        </table>
      </div>

      {resultImage ? (
        <div>
          <p className="text-center font-semibold text-xl mb-8">
            Ảnh trích xuất được
          </p>
          <div className="space-y-1">
            <div className="flex justify-center md:px-8">
              <img alt="Result image" src={URL.createObjectURL(resultImage)} />
            </div>
          </div>
        </div>
      ) : // <div className="flex justify-center md:p-8">
      //   <img alt="Result image" src={URL.createObjectURL(selectedImage)} />
      // </div>
      null}
    </>
  )
}

export default CongVan
