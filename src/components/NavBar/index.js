const NavBar = () => {
  return (
    <div>
      <nav>
        <div className="flex space-x-4 items-center">
          <img
            className="object-fit w-12 h-12"
            src="images/logo-uit.png"
            alt="GuRu Reader"
          />
          <h1 className="font-semibold text-3xl">GuRu Reader</h1>
        </div>
      </nav>
    </div>
  )
}

export default NavBar
