const Footer = () => {
  return (
    <footer
      className='w-full flex justify-center items-center h-12 text-white'
      style={{ backgroundColor: '#4b5563' }}
    >
      <div>Copyright by AICLUB - UIT @ {new Date().getFullYear()}</div>
    </footer>
  )
}

export default Footer
