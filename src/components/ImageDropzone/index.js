import axios from '@/axios-instance'
import { DataContext } from '@context/index'
import { useCallback, useContext, useState } from 'react'
import { useDropzone } from 'react-dropzone'

const ImageDropzone = ({ inputURLRef }) => {
  const { setErrorMessage, errorMessage, selectedImage, setSelectedImage } =
    useContext(DataContext)

  const [loadingImage, setLoadingImage] = useState(false)

  // const resizeImage = async (image, name, type) => {
  //   image = await fromImage(image)
  //   let { originalWidth, originalHeight } = image
  //   if (originalWidth > 750) {
  //     image = image.scale(750, (750 / originalWidth) * originalHeight)
  //   }
  //   return image.quality(1.0).type(type).toFile(name)
  // }

  const onDrop = useCallback(async (acceptedFiles) => {
    if (acceptedFiles && acceptedFiles.length === 1) {
      let image = acceptedFiles[0]
      // image = await resizeImage(image, image.name, image.type)
      setSelectedImage(image)
    }
  }, [])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: 'image/jpeg, image/png, application/pdf',
    maxFiles: 1,
  })

  const isValidURL = (str) => {
    try {
      let url = new URL(str)
      return true
    } catch (_) {
      return false
    }
  }

  const downloadImage = async (url) => {
    let response = await axios.post(
      '/download_image',
      {
        path: url,
      },
      {
        responseType: 'blob',
      }
    )
    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service downloadImage')
      return
    }
    if (!response.data.type.includes('image')) {
      setErrorMessage('URL không tồn tại hoặc file không phải là ảnh hợp lệ!')
      return
    }
    return response.data
  }

  const handleURLInput = async (_) => {
    setErrorMessage(null)
    const url = inputURLRef.current.value
    if (!isValidURL(url)) {
      setErrorMessage('URL không hợp lệ!')
      return
    }
    setLoadingImage(true)
    let image = await downloadImage(url)
    setLoadingImage(false)
    if (image) setSelectedImage(image)
  }

  const className =
    'w-56 h-56 p-4 border-4 border-dashed text-center flex justify-center items-center'
  const selectedClassName = 'text-center flex justify-center items-center'

  return (
    <div className="w-full flex flex-col items-center space-y-4">
      {errorMessage && (
        <div className="text-lg text-red-500 text-semibold">
          <p>{errorMessage}</p>
        </div>
      )}
      <div
        className={selectedImage ? selectedClassName : className}
        {...getRootProps()}
      >
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Thả file tại đây</p>
        ) : (
          <p>{!selectedImage && 'Kéo thả ảnh tại đây hoặc nhấn để chọn'}</p>
        )}

        {selectedImage && (
          <div className="md:p-8">
            {!selectedImage.type.includes('pdf') ? (
              <img
                alt="Selected image"
                src={URL.createObjectURL(selectedImage)}
              />
            ) : (
              <div className="text-xl font-semibold">
                File: {selectedImage.name}
              </div>
            )}
          </div>
        )}
      </div>

      <input
        className="w-72 px-2 border-2 border-gray-300 rounded"
        placeholder="Tải ảnh thông qua đường dẫn (URL)"
        onChange={handleURLInput}
        ref={inputURLRef}
      />

      {loadingImage && (
        <div>
          <p>Đang tải ảnh</p>
        </div>
      )}
    </div>
  )
}

export default ImageDropzone
