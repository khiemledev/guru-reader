// import Demo1 from '@assets/images/test1.jpg'
// import Demo2 from '@assets/images/test2.jpg'
// import Demo3 from '@assets/images/test3.jpg'
// import Demo4 from '@assets/images/test4.jpg'
// import Demo5 from '@assets/images/test5.jpg'
// import Demo6 from '@assets/images/test6.jpg'
import Demo1 from '@assets/images/docile/test1.png'
import Demo2 from '@assets/images/docile/test2.png'
import Demo3 from '@assets/images/docile/test3.png'
import Demo4 from '@assets/images/docile/test4.png'
import Demo5 from '@assets/images/docile/test5.png'
import Demo6 from '@assets/images/docile/test6.png'
import { DataContext } from '@context/index'
import { fromImage } from 'imtool'
import Image from 'next/image'
import { useContext } from 'react'

const Mockup = () => {
  const { setSelectedImage, setDocTypeId } = useContext(DataContext)

  const demoImages = [
    // {
    //   src: Demo1,
    //   docTypeId: 'bang-diem-thpt',
    // },
    // {
    //   src: Demo2,
    //   docTypeId: 'cong-van',
    // },
    // {
    //   src: Demo3,
    //   docTypeId: 'bang-diem-thpt',
    // },
    // {
    //   src: Demo4,
    //   docTypeId: 'cong-van',
    // },
    // {
    //   src: Demo6,
    //   docTypeId: 'bang-diem-thpt',
    // },
    // {
    //   src: Demo5,
    //   docTypeId: 'cong-van',
    // },
    {
      src: Demo1,
      docTypeId: 'docile',
    },
    {
      src: Demo2,
      docTypeId: 'docile',
    },
    {
      src: Demo3,
      docTypeId: 'docile',
    },
    {
      src: Demo4,
      docTypeId: 'docile',
    },
    {
      src: Demo5,
      docTypeId: 'docile',
    },
    {
      src: Demo6,
      docTypeId: 'docile',
    },
  ]

  const setImage = async (image, name, docTypeId) => {
    image = await fromImage(window.location.href + image.src)
    image = await image.quality(0.95).toFile(name)
    setDocTypeId(docTypeId)
    setSelectedImage(image)
  }

  return (
    <div className="space-y-20">
      <h3 className="text-2xl font-semibold">Demo images</h3>

      <div className="grid grid-flow-col grid-cols-3 grid-rows-2 gap-2">
        {demoImages.map((e, i) => (
          <div className="w-full relative h-40">
            <Image
              key={i}
              onClick={() => setImage(e.src, `demo_${i + 1}.jpg`, e.docTypeId)}
              src={e.src}
              alt={i}
              objectFit="contain"
              layout="fill"
              className="cursor-pointer"
            />
          </div>
        ))}
      </div>

      {/* <div className="space-y-12">
        <p className="flex justify-center text-xl uppercase">
          Một số lưu ý khi tải lên
        </p>
        <div className="space-y-4">
          <p>Vui lòng chụp ảnh rõ nét không bị che khuất.</p>
          <p>Thẻ chụp phải còn đầy đủ bốn gốc.</p>
        </div>
      </div> */}
    </div>
  )
}

export default Mockup
