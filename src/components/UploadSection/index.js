import axios from '@/axios-instance'
import {
  cropImage,
  // encodeImageBase64,
  decodeImageBase64,
  drawBBox,
} from '@/utils/image_utils'
import ImageDropzone from '@components/ImageDropzone'
import { DataContext } from '@context/index'
import Cookies from 'js-cookie'
import Script from 'next/script'
import { useContext, useRef, useState } from 'react'

const SERVICE_URL = process.env.NEXT_PUBLIC_SERVICE_URL
const TOKEN = process.env.NEXT_PUBLIC_TOKEN

const docTypes = [
  // {
  //   id: 'chung',
  //   text: 'Chung',
  // },
  // {
  //   id: 'book',
  //   text: 'Sách',
  // },
  // {
  //   id: 'administrative',
  //   text: 'VB Hành chính',
  // },
  // {
  //   id: 'hoa-don-mua-hang',
  //   text: 'Hóa đơn mua hàng',
  // },
  // {
  //   id: 'bang-diem-thpt',
  //   text: 'Bảng điểm THPT',
  // },
  // {
  //   id: 'the-bhyt',
  //   text: 'Thẻ BHYT',
  // },
  // {
  //   id: 'giay-khai-sinh',
  //   text: 'Giấy Khai Sinh',
  // },
  // {
  //   id: 'the-ngan-hang',
  //   text: 'Thẻ Ngân Hàng',
  // },
  // {
  //   id: 'giay-phep-kinh-doanh',
  //   text: 'Giấy Phép Kinh Doanh',
  // },
  // {
  //   id: 'the-mtq',
  //   text: 'Thẻ MTQ',
  // },
  // {
  //   id: 'card-visit',
  //   text: 'Card Visit',
  // },
  // {
  //   id: 'cong-van',
  //   text: 'Công Văn',
  // },
  {
    id: 'docile',
    text: 'DOCILE',
  },
]

const itemClassName =
  'm-0.5 bg-white border-2 border-blue-500 rounded flex items-center text-center justify-center text-sm px-2 py-0.5 cursor-pointer'
const itemClassNameSelected =
  'm-0.5 bg-blue-500 text-white border-2 border-blue-500 rounded flex items-center text-center justify-center text-sm px-2 py-0.5 cursor-pointer'
const btnClassName =
  'inline-block bg-blue-500 text-white py-2 w-40 rounded-full cursor-pointer'

const UploadSection = () => {
  const {
    selectedImage,
    setSelectedImage,
    setResultImage,
    setExtractedInfo,
    docTypeId,
    setDocTypeId,
    processing,
    setProcessing,
    setErrorMessage,
  } = useContext(DataContext)

  const [reCAP, setReCAP] = useState(true)

  const inputURLRef = useRef(null)

  const extractReceipt = async () => {
    const formData = new FormData()
    formData.append('file', selectedImage)
    let response = await axios.post(
      `${SERVICE_URL}/receipt/extract`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    )
    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service')
      return
    }

    response = response.data
    if (response['code'] !== '1000') {
      setErrorMessage(`Đã xảy ra lỗi. Mã lỗi ${response['code']}`)
      return
    }

    response = response['data']
    const croppedImage = await decodeImageBase64(response['image_base64'][0])
    setResultImage(await drawBBox(croppedImage, response['bb_field']))
    setExtractedInfo(response)
  }

  const extractHighSchoolScore = async () => {
    // const imageBase64 = (await encodeImageBase64(selectedImage)).split(',')[1]
    const formData = new FormData()
    formData.append('info_type', '111')
    formData.append('file', selectedImage)
    let response = await axios.post(
      `${SERVICE_URL}/doc/highschool_graduation_score_multipart`,
      // {
      //   imageBase64: imageBase64,
      // },
      formData,
      {
        headers: {
          // 'Content-Type': 'application/json',
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    )
    if (response.status !== 200) {
      setErrorMessage('Không thể kết nố đến service')
      return
    }

    response = response.data
    if (response['code'] !== '200') {
      console.error(response)
      setErrorMessage(`Đã xảy ra lỗi. Mã lỗi ${response['code']}`)
      return
    }

    setExtractedInfo(response['data'])
    setResultImage([
      await cropImage(selectedImage, response['data']['barcode']['bbox']),
      await cropImage(selectedImage, response['data']['grades']['bbox']),
    ])
  }

  const extractGeneral = async () => {
    const formData = new FormData()
    formData.append('file', selectedImage)
    let response = await axios.post(`${SERVICE_URL}/doc/general`, formData, {
      Authorization: `Bearer ${TOKEN}`,
    })

    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service doc/general')
      return
    }

    response = response.data
    if (response['code'] !== '1000') {
      setErrorMessage(`Đã có lỗi xảy ra. Mã lỗi ${response['code']}`)
      return
    }

    response = response['data']
    console.log(response)
    setResultImage([selectedImage])
    setExtractedInfo(response)
  }

  const extractAdministrative = async () => {
    const formData = new FormData()
    formData.append('file', selectedImage)
    let response = await axios.post(
      `${SERVICE_URL}/doc/administrative`,
      formData,
      {
        Authorization: `Bearer ${TOKEN}`,
      }
    )

    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service doc/administrative')
      return
    }

    response = response.data
    if (response['code'] !== '1000') {
      setErrorMessage(`Đã có lỗi xảy ra. Mã lỗi ${response['code']}`)
      return
    }

    response = response['data']
    setResultImage([selectedImage])
    setExtractedInfo(response)
  }

  const extractCardVisit = async () => {
    const formData = new FormData()
    formData.append('file', selectedImage)
    let response = await axios.post(`${SERVICE_URL}/doc/card_visit`, formData, {
      Authorization: `Bearer ${TOKEN}`,
    })

    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service doc/card_visit')
      return
    }

    response = response.data
    if (response['code'] !== '1000') {
      setErrorMessage(`Đã có lỗi xảy ra. Mã lỗi ${response['code']}`)
      return
    }

    response = response['data']
    const croppedImage = await decodeImageBase64(response['img_base64'])
    setResultImage(croppedImage)
    setExtractedInfo(response)
  }

  const extractCongVan = async () => {
    const formData = new FormData()
    formData.append('file', selectedImage)
    formData.append('threshold', '0.5')
    let response = await axios.post(
      `${SERVICE_URL}/congvan/extract`,
      formData,
      {
        Authorization: `Bearer ${TOKEN}`,
      }
    )

    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service doc/card_visit')
      return
    }

    response = response.data
    if (response['code'] != 1000) {
      setErrorMessage(`Đã có lỗi xảy ra. Mã lỗi ${response['code']}`)
      return
    }

    response = response['data'][0]
    setExtractedInfo(response)

    if (!Array.isArray(response.img_base64)) {
      const croppedImage = await decodeImageBase64(response['img_base64'])
      setResultImage(croppedImage)
    }
  }

  const extractDOCILE = async () => {
    // const imageBase64 = (await encodeImageBase64(selectedImage)).split(',')[1]
    const formData = new FormData()
    formData.append('file', selectedImage)
    let response = await axios.post(
      `${SERVICE_URL}/docile/extract`,
      // {
      //   imageBase64: imageBase64,
      // },
      formData,
      {
        responseType: 'blob',
        headers: {
          // 'Content-Type': 'application/json',
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    )
    if (response.status !== 200) {
      setErrorMessage('Không thể kết nố đến service')
      return
    }
    setExtractedInfo({ docType: 'docile' })
    console.log(response.data)
    setResultImage([response.data])
  }

  const uploadFile = async (e) => {
    e.preventDefault()

    await verifyVCaptcha()

    if (!reCAP) {
      setErrorMessage('Vui lòng thực hiện reCAPTCHA!')
      return
    }

    setProcessing(true)
    setErrorMessage(null)
    setExtractedInfo(null)
    setResultImage(null)

    try {
      if (docTypeId === 'chung' || docTypeId === 'book') await extractGeneral()
      if (docTypeId === 'administrative') await extractAdministrative()
      if (docTypeId === 'hoa-don-mua-hang') await extractReceipt()
      if (docTypeId === 'bang-diem-thpt') await extractHighSchoolScore()
      if (docTypeId === 'card-visit') await extractCardVisit()
      if (docTypeId === 'cong-van') await extractCongVan()
      if (docTypeId === 'docile') await extractDOCILE()
    } catch (err) {
      console.error(err)
      setErrorMessage('Đã xảy ra lỗi không xác định. Vui lòng thử lại!')
    } finally {
      setProcessing(false)
    }
  }

  // const onCAPTCHAChange = (v) => {
  //   if (v) setReCAP(true)
  // }

  // const onCAPTCHAExpired = () => {
  //   setReCAP(false)
  //   setErrorMessage('reCAPTCHA hết hạn, vui lòng thực hiện lại!')
  // }

  // const onCAPTCHAErrored = () => {
  //   setReCAP(false)
  //   setErrorMessage('Đã xảy ra lỗi khi thực hiện reCAPTCHA, vui lòng thử lại!')
  // }

  const verifyVCaptcha = async () => {
    const sessionId = Cookies.get('vcaptcha_session_id')
    const response = await axios.post(
      'https://aiclub.uit.edu.vn/captcha/demo/validate_captcha',
      {
        secret: 'captcha_secret',
        private_key: '123456',
        session_id: sessionId,
        hostname: 'aiclub.uit.edu.vn',
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    if (response.status !== 200) {
      setErrorMessage('Không thể kết nối đến service vcaptcha')
      return
    }
    const data = response.data
    if (data.code != 1000) {
      setErrorMessage('Xác thực captcha thất bại')
      return
    }
    if (!data.success) {
      setErrorMessage('Xác thực captcha thất bại')
      return
    }
    Cookies.remove('vcaptcha_session_id')
    setReCAP(true)
  }

  const reset = () => {
    setResultImage(null)
    setExtractedInfo(null)
    setSelectedImage(null)
    setErrorMessage(null)
    inputURLRef.current.value = ''
  }

  const changeDocType = (id) => {
    setDocTypeId(id)
    reset()
  }

  return (
    <div className="space-y-12">
      <Script
        src="https://aiclub.uit.edu.vn/vcaptcha/build/bundle.js"
        strategy="afterInteractive"
      ></Script>
      <div>
        <ul className="flex flex-wrap">
          {docTypes.map((e) => {
            return (
              <li
                key={e.id}
                onClick={() => !processing && changeDocType(e.id)}
                className={
                  e.id == docTypeId ? itemClassNameSelected : itemClassName
                }
              >
                {e.text}
              </li>
            )
          })}
        </ul>
      </div>

      <div className="w-full flex justify-center">
        <ImageDropzone inputURLRef={inputURLRef} />
      </div>

      <form
        onSubmit={
          !processing && selectedImage ? uploadFile : (e) => e.preventDefault()
        }
        className="text-center space-y-3"
      >
        <div className="flex justify-center">
          {/* <ReCAPTCHA
            sitekey="6LeA7LocAAAAAMcJ6aRO_wG0HdwsEnhOpxaMW3Wq"
            onChange={onCAPTCHAChange}
            onExpired={onCAPTCHAExpired}
            onErrored={onCAPTCHAErrored}
          ></ReCAPTCHA> */}
          {/* <Head>
            <link
              rel="stylesheet"
              type="text/css"
              href="https://aiclub.uit.edu.vn/vcaptcha/build/bundle.css"
            ></link>
          </Head>
          <div id="vcaptcha-body"></div> */}
        </div>
        <input
          className={processing ? btnClassName + ' bg-gray-500' : btnClassName}
          type="submit"
          value={processing ? 'Đang xử lý...' : 'Xử lý'}
        ></input>

        <br />
        <div onClick={reset} className={btnClassName + ' bg-gray-600'}>
          <button>Chọn lại</button>
        </div>
      </form>
    </div>
  )
}

export default UploadSection
