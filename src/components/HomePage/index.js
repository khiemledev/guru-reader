import UploadSection from '@components/UploadSection'
import ResultSection from '@components/ResultSection'

const HomePage = () => {
  return (
    <div className="md:flex space-y-20 md:space-y-0 md:divide-solid md:divide-x-2 md:divide-gray-300">
      <div className="md:flex-1 md:pr-10">
        <UploadSection />
      </div>
      <div className="md:flex-1 md:pl-10">
        <ResultSection />
      </div>
    </div>
  )
}

export default HomePage
