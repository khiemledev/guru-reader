import { useContext, useRef, useEffect, useState } from 'react'
import { DataContext } from '@context/index'
import { fromImage } from 'imtool'
import ReactTooltip from 'react-tooltip'

const CardVisit = () => {
  const { selectedImage, resultImage, extractedInfo } = useContext(DataContext)
  const [text, setText] = useState('')
  const imgCanvas = useRef(null)
  const bboxCanvas = useRef(null)
  let offsetX = 0,
    offsetY = 0

  const fields = [
    {
      name: 'name',
      label: 'Họ tên',
    },
    {
      name: 'mobile',
      label: 'Số điện thoại',
    },
    {
      name: 'address',
      label: 'Địa chỉ',
    },
    {
      name: 'email',
      label: 'Email',
    },
    {
      name: 'website',
      label: 'Website',
    },
  ]

  const reOffset = () => {
    if (!bboxCanvas.current) return
    let bbox = bboxCanvas.current.getBoundingClientRect()
    offsetX = bbox.left
    offsetY = bbox.top
  }

  const drawBbox = (ctx, bb) => {
    ctx.beginPath()
    ctx.moveTo(bb[0], bb[1])
    ctx.lineTo(bb[2], bb[3])
    ctx.lineTo(bb[4], bb[5])
    ctx.lineTo(bb[6], bb[7])
    ctx.closePath()
  }

  const onMouseHover = (e, ctx, imW, imH, cvW, cvH) => {
    e.preventDefault()
    e.stopPropagation()

    let mouseX = parseInt(e.clientX - offsetX)
    let mouseY = parseInt(e.clientY - offsetY)

    ctx.clearRect(0, 0, cvW, cvH)

    let mouseInText
    extractedInfo['results']['texts'].forEach((e) => {
      let bb = e[0]
      bb = bb.map((e) => e * (cvW / imW))
      drawBbox(ctx, bb)
      if (ctx.isPointInPath(mouseX, mouseY)) {
        ctx.fillStyle = 'rgba(0, 200, 0, 0.45)'
        ctx.fill()
        ctx.lineWidth = 2
        ctx.strokeStyle = 'rgba(255, 0, 0, 1.0)'
        ctx.stroke()
        mouseInText = e[1]
      } else {
        ctx.strokeStyle = 'rgba(0, 200, 0, 1.0)'
        ctx.stroke()
      }
    })
    if (mouseInText) {
      if (mouseInText !== text) setText(mouseInText)
    } else {
      setText('')
    }
  }

  useEffect(() => {
    const interval = setInterval(reOffset, 1000)

    return () => {
      clearInterval(interval)
    }
  }, [])

  useEffect(async () => {
    if (!extractedInfo) return

    let img = await fromImage(resultImage ?? selectedImage)
    let { width: imW, height: imH } = img

    let width = imgCanvas.current.offsetWidth
    let height = (width / imW) * imH
    imgCanvas.current.width = width
    imgCanvas.current.height = height
    bboxCanvas.current.width = width
    bboxCanvas.current.height = height

    let imgEle = await img.toImage()

    let imCtx = imgCanvas.current.getContext('2d')
    imCtx.drawImage(imgEle, 0, 0, width, height)

    let bboxCtx = bboxCanvas.current.getContext('2d')
    bboxCtx.fillStyle = 'rgba(0, 200, 0, 0.2)'
    extractedInfo['results']['texts']
      .map((e) => e[0])
      .forEach((e) => {
        e = e.map((e) => e * (width / imW))
        drawBbox(bboxCtx, e)
        bboxCtx.fill()
      })

    reOffset()

    bboxCanvas.current.onmousemove = (e) => {
      onMouseHover(e, bboxCtx, imW, imH, width, height)
    }
  }, [extractedInfo])

  const postProcess = (text) => {
    let t = text.toLowerCase()
    const needToReplaces = [
      'số',
      'chuyển',
      'đến',
      'ngày',
      's6',
      'kính gửi',
      'đồng kính gửi',
      ':',
    ]
    for (let textToReplace of needToReplaces) {
      if (t.startsWith(textToReplace)) {
        text = text.substring(textToReplace.length)
        t = text.toLowerCase()
      }
    }
    return text.trim()
  }

  return (
    <>
      <div className="space-y-12">
        <div className="text-xl text-center">Kết quả</div>
      </div>

      <table className="mx-auto text-lg">
        <tbody>
          {fields.map(({ name, label }, index) => {
            let first = true
            return extractedInfo['extracted_info'].map((r, index2) => {
              if (r[0] != name) return
              if (!r[1]) return
              if (first) {
                first = false
                return (
                  <tr key={index + index2}>
                    <td
                      className="p-2 pr-5 text-right font-bold"
                      style={{ minWidth: '150px' }}
                    >
                      {label}
                    </td>
                    <td className="p-2 text-left">{r[1]}</td>
                  </tr>
                )
              }
              return (
                <tr key={index + index2}>
                  <td></td>
                  <td className="p-2 text-left">{r[1]}</td>
                </tr>
              )
            })
          })}
        </tbody>
      </table>

      {resultImage ? (
        <div className="space-y-8">
          <div className="p-8">
            <div className="flex justify-center md:px-8 relative">
              {/* <img
                alt="Result image"
                src={URL.createObjectURL(resultImage[0])}
              /> */}
              <canvas
                className="w-full absolute top-0 left-0 z-0"
                ref={imgCanvas}
              ></canvas>
              <canvas
                className="w-full absolute top-0 left-0 z-10"
                ref={bboxCanvas}
                data-tip={text}
              ></canvas>

              <ReactTooltip>
                {text !== '' && <p className="text-xl uppercase">{text}</p>}
              </ReactTooltip>
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center md:p-8">
          <img alt="Result image" src={URL.createObjectURL(selectedImage)} />
        </div>
      )}
    </>
  )
}

export default CardVisit
