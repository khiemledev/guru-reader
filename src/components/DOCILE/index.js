import { DataContext } from '@context/index'
import { useContext } from 'react'

const DOCILE = () => {
  const { selectedImage, resultImage } = useContext(DataContext)

  return (
    <>
      {resultImage ? (
        <div className="space-y-8">
          <div className="text-xl text-center">Kết quả</div>
          <div>
            {/* <p className="text-center font-semibold text-xl">Barcode</p> */}
            <div className="flex justify-center md:px-8">
              <img
                alt="Result image"
                src={URL.createObjectURL(resultImage[0])}
              />
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center md:p-8">
          <img alt="Result image" src={URL.createObjectURL(selectedImage)} />
        </div>
      )}
    </>
  )
}

export default DOCILE
