import { DataContext } from '@context/index'
import { useContext } from 'react'
import Administrative from '../Administrative'
import CardVisit from '../CardVisit'
import CongVan from '../CongVan'
import DOCILE from '../DOCILE'
import General from '../General'
import HighSchoolScore from '../HighSchoolScore'
import ReceiptResult from '../ReceiptResult'

const ResultView = () => {
  const { docTypeId } = useContext(DataContext)

  return (
    <div className="space-y-12">
      {docTypeId === 'chung' && <General />}
      {/* {docTypeId === 'book' && <Book />} */}
      {docTypeId === 'administrative' && <Administrative />}
      {docTypeId === 'hoa-don-mua-hang' && <ReceiptResult />}
      {docTypeId === 'bang-diem-thpt' && <HighSchoolScore />}
      {docTypeId === 'card-visit' && <CardVisit />}
      {docTypeId === 'cong-van' && <CongVan />}
      {docTypeId === 'docile' && <DOCILE />}
    </div>
  )
}

export default ResultView
