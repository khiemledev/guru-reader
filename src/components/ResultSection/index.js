import { DataContext } from '@context/index'
import { useContext } from 'react'
import Mockup from '@components/Mockup'
import ResultView from '@components/ResultView'

const ResultSection = () => {
  const { extractedInfo } = useContext(DataContext)

  return <div className="h-full overflow-y-scroll">{extractedInfo ? <ResultView />  : <Mockup />}</div>
}

export default ResultSection
