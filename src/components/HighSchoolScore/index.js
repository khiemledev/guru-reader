import { DataContext } from '@context/index'
import { useContext } from 'react'

const HighSchoolScore = () => {
  const { selectedImage, resultImage, extractedInfo } = useContext(DataContext)

  const fields = [
    {
      name: 'toan',
      label: 'Toán',
    },
    {
      name: 'van',
      label: 'Văn',
    },
    {
      name: 'ngoaingu',
      label: 'NN',
    },
    {
      name: 'li',
      label: 'Lý',
    },
    {
      name: 'hoa',
      label: 'Hóa',
    },
    {
      name: 'sinh',
      label: 'Sinh',
    },
    {
      name: 'su',
      label: 'Sử',
    },
    {
      name: 'dia',
      label: 'Địa',
    },
    {
      name: 'gdcd',
      label: 'GDCD',
    },
  ]

  return (
    <>
      <div className="space-y-12">
        <div className="text-xl text-center">Kết quả</div>
        <table className="mx-auto text-lg">
          <tbody>
            <tr>
              <td className="p-2 pr-5 text-right font-bold">Barcode</td>
              <td className="p-2 text-left">
                {extractedInfo['barcode']['content']}
              </td>
            </tr>
            <tr>
              <td className="p-2 pr-5 text-right font-bold">Số CMTND/CCCD</td>
              <td className="p-2 text-left">
                {extractedInfo['idnum']['content']}
              </td>
            </tr>
            {fields.map(({ name, label }, index) => {
              if (extractedInfo['grades']['content'][name]) {
                return (
                  <tr key={index}>
                    <td className="p-2 pr-5 text-right font-bold">{label}</td>
                    <td className="p-2 text-left">
                      {extractedInfo['grades']['content'][name]}
                    </td>
                  </tr>
                )
              }
              return null
            })}
          </tbody>
        </table>
      </div>

      {resultImage ? (
        <div className="space-y-8">
          <div>
            <p className="text-center font-semibold text-xl">Barcode</p>
            <div className="flex justify-center md:px-8">
              <img
                alt="Result image"
                src={URL.createObjectURL(resultImage[0])}
              />
            </div>
          </div>
          <div>
            <p className="text-center font-semibold text-xl">Table</p>
            <div className="flex justify-center md:px-8">
              <img
                alt="Result image"
                src={URL.createObjectURL(resultImage[1])}
              />
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center md:p-8">
          <img alt="Result image" src={URL.createObjectURL(selectedImage)} />
        </div>
      )}
    </>
  )
}

export default HighSchoolScore
